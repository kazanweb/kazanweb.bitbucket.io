$(document).ready(function () {

    window.globalPopup = new Popup();

	var dataJson;

	$.getJSON(pathJsonData, function(response) {

		dataJson = response;

		var catalogThumbs = $('.js-catalog-thumb');

		$(catalogThumbs).each(function (index) {

			var counter = 1;

			var slider = $(this).find('.js-catalog-thumb__slider');
			var select = $(this).find('.js-combox-button__select');
			var renewal = $(this).find('.js-checkbox__input');
			var price = $(this).find('.js-price');
			var save = $(this).find('.js-save');
			var button = $(this).find('.js-catalog-thumb__buy');
			var hintPopup = $(this).find('.js-catalog-thumb__hint');
			var trigger = this.getAttribute('data-product');
			var unit = dataJson[0].data[trigger][select[0].value].unit;

			var calculate = function() {

				if(trigger == 'Total') {
					$(save).html((parseFloat(dataJson[0].data[trigger][select[0].value].auto[counter].price) / 12 * 3).toFixed(2) + '&nbsp;' + unit);
				}

				if(renewal[0].checked) {
					$(price).html(dataJson[0].data[trigger][select[0].value].auto[counter].price);
					button[0].setAttribute('href', dataJson[0].data[trigger][select[0].value].auto[counter].url);
					button[0].setAttribute('ipmparam', dataJson[0].data[trigger][select[0].value].auto[counter].url);
					return false;
				}

				$(price).html(dataJson[0].data[trigger][select[0].value].manual[counter].price);
				button[0].setAttribute('href', dataJson[0].data[trigger][select[0].value].manual[counter].url);
				button[0].setAttribute('ipmparam', dataJson[0].data[trigger][select[0].value].manual[counter].url);

			};

			var hint;
			var parent;

			$(slider).slider({
				min: parseInt(slider[0].getAttribute('data-min')),
				max: parseInt(slider[0].getAttribute('data-max')),
				value: parseInt(slider[0].getAttribute('data-default-value')),
				range: 'min',
				create: function() {

					$(this).find('.ui-slider-handle').append('<span class="ui-slider-hint">' + parseInt(slider[0].getAttribute('data-default-value')) + '</span>');

					parent = $(this).parents('.js-catalog-thumb');
					hint = $(this).find('.ui-slider-hint');
					counter = parseInt(slider[0].getAttribute('data-default-value'));

				},
				change: function(event, ui) {

					$(hint).html(ui.value);
					counter = ui.value;

					if(ui.value == 0) {
						$(obj).slider('value', 1);
						$(hint).html('1');
						counter = 1;
					}

					calculate();

				},
				slide: function(event, ui) {

					$(hint).html(ui.value);
					counter = ui.value;

					if(ui.value == 0) {
						$(obj).slider('value', 1);
						$(hint).html('1');
						counter = 1;
					}

					calculate();

				}
			});

			$(select).change(function() {
				unit = this.value;
				calculate();
			});

			$(renewal).change(function () {
				calculate();
			});

			$(hintPopup).click(function() {
				$.get(this.getAttribute('href'), function (response) {
					globalPopup.html(response).show();

					$('.js-renewal__popup').click(function () {
						globalPopup.close();
						renewal[0].checked = true;
						calculate();
						return false;
					});

					$('.js-renewal__popupcancel').click(function () {
						globalPopup.close();
						renewal[0].checked = false;
						calculate();
						return false;
					});

				});
				return false;
			});

		});
	});

});